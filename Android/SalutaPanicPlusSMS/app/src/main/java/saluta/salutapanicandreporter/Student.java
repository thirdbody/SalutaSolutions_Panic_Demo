package saluta.salutapanicandreporter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Simple data structure for student contact info.
 */

public class Student implements java.io.Serializable {
    private String m_name;
    private String m_number;

    public Student(String name, String number) {
        m_name = name;
        m_number = number;
    }

    public Student(String combo) {
        String[] stringCombo = combo.split(", ");
        m_name = stringCombo[0];
        m_number = stringCombo[1];
    }

    public String[] getAllInfo() {
        String[] infoArray = {m_name, m_number};
        return infoArray;
    }
    public String getName() {
        return m_name;
    }
    public String getNum() { return m_number; }

    public void serializeStudent() {
        try {
            FileOutputStream fout = new FileOutputStream("thestudent.dat");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(this);
            oos.close();
        }
        catch (Exception e) { e.printStackTrace(); }
    }
    public static void deserlializeStudent(Student student) {
        File file=new File("thestudent.dat");

        try {

            FileInputStream fint = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fint);
            Student stud=(Student) ois.readObject();
            ois.close();
        }
        catch (Exception e) { e.printStackTrace(); }
    }
}
